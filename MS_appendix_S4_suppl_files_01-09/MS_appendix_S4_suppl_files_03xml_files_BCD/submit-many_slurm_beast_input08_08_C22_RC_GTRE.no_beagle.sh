#!/bin/bash

### Job parameters

JOB_NAME=beast_input08_08_C22_RC_GTRE
INPUT_FILE=${JOB_NAME}".xml"
WALL_CLOCK_LIMIT=48:00:00
MEMORY=2gb
NOTIFY=ALL
ITERATIONS=6
CPUS_PER_JOB=2
### Check for input file

if [ ! -e $INPUT_FILE ]; then
echo "$INPUT_FILE doesn't exist"
fi

### Get memory and virtual memory for ulimit
mem_gb=${MEMORY%gb}
let "mem_kb=${mem_gb}*1024*1024"
let "mem_mb=${mem_gb}*1024"

EXE="java -Xmx${mem_gb}g -Djava.library.path=/share/apps/BEAST/noarch/1.8.2/lib -cp /share/apps/BEAST/noarch/1.8.2/lib/beast.jar dr.app.beast.BeastMain"

### EXE="beast -beagle_off -threads $CPUS_PER_JOB"
###  see: https://groups.google.com/forum/#!msg/beast-users/9zhYfoT8VTQ/Hky7xXYniAwJ
EXE="beast -beagle_off -threads 1"
### Job creation

for (( i=0; i<$ITERATIONS; i++ )); do

DIR=$PWD #/$JOB_NAME"-0$i"

#if [ ! -d $DIR ]; then
#mkdir $DIR
#fi

FNAME=$( echo $INPUT_FILE | sed -e 's/.xml/-0'$i'.xml/g' )

cp $INPUT_FILE $DIR/$FNAME

sed -i 's/'$JOB_NAME'/'$JOB_NAME'-0'$i'/g' $DIR/$FNAME

JOBFILE=$DIR/$JOB_NAME"-0$i.sl"

echo "#!/bin/bash" > $JOBFILE
echo "#SBATCH -J $JOB_NAME-0$i" >> $JOBFILE
### echo "#SBATCH --mail-user=sknu003@auckland.ac.nz" >> $JOBFILE
echo "#SBATCH --mail-type=$NOTIFY" >> $JOBFILE
echo "#SBATCH --cpus-per-task=$CPUS_PER_JOB" >> $JOBFILE
echo "#SBATCH --ntasks=1" >> $JOBFILE
echo "#SBATCH -A uoa00029" >> $JOBFILE 
echo "#SBATCH --time=$WALL_CLOCK_LIMIT" >> $JOBFILE
echo "#SBATCH --mem-per-cpu=${mem_mb}" >> $JOBFILE
echo "#SBATCH -D $DIR" >> $JOBFILE
echo "#SBATCH -o $DIR/%j.%N.out" >> $JOBFILE
echo "#SBATCH -e $DIR/%J.%N.err" >> $JOBFILE

echo "module load BEAST/1.8.2-goolf-1.5.14" >> $JOBFILE
echo "srun $EXE $FNAME" >> $JOBFILE

sbatch $JOBFILE

done
