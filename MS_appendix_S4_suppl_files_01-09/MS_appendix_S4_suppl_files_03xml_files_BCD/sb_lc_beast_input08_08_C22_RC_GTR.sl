#!/bin/bash
#SBATCH -J lc_beast_input08_08_C22_RC_GTR
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /home/sknu003/kyp_project/beast_input08_08_C22_RC_GTR
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_lc_beast_input08_08_C22_RC_GTR.txt
#SBATCH -e stderr_lc_beast_input08_08_C22_RC_GTR.txt
#SBATCH -C sb               # sb=Sandybridge,wm=Westmere


module load BEAST/1.8.0
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -trees *.species.trees beast_input08_08_C22_RC_GTR_comb3.trees
srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -trees -burnin 8000000 *.trees beast_input08_08_C22_RC_GTR_comb3.trees