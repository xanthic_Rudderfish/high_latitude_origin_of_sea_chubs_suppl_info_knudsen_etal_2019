#!/bin/bash
#SBATCH -J ta_beast_input08_08_D41_RC_GTR
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /home/sknu003/kyp_project/beast_input08_08_D41_RC_GTR
#SBATCH --time=1:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_ta_beast_input08_08_D41_RC_GTR.txt
#SBATCH -e stderr_ta_beast_input08_08_D41_RC_GTR.txt
#SBATCH -C sb               # sb=Sandybridge,wm=Westmere

module load java
srun java -Xms64m -Xmx4000m -Djava.library.path=/share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/  -cp /share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/beast.jar dr.app.tools.TreeAnnotator -heights mean beast_input08_08_D41_RC_GTR_comb3.trees beast_input08_08_D41_RC_GTR_mean_tree.tre
