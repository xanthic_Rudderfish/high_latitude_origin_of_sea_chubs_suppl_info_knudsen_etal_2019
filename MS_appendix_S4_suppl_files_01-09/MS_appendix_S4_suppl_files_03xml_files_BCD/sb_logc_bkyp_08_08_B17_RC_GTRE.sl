#!/bin/bash
#SBATCH -J logc_beast_input08_08_B17_RC_GTRE
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /home/sknu003/kyp_project/beast_input08_08_B17_RC_GTRE
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_logc_beast_input08_08_B17_RC_GTRE.txt
#SBATCH -e stderr_logc_beast_input08_08_B17_RC_GTRE.txt
#SBATCH -C sb               # sb=Sandybridge,wm=Westmere

module load BEAST/1.8.0
### srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 32000000 *.log beast_input08_08_B17_RC_GTRE_comb.log
### srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 0 *.log beast_input08_08_B17_RC_GTRE_comb.log
srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 8000000 *.log beast_input08_08_B17_RC_GTRE_comb.log