#!/bin/bash
# -*- coding: utf-8 -*-

##get the present directory
WD=$(pwd)

##BSTR_N="14"
mkdir starBEAST_kyp096_009E
mkdir starBEAST_kyp096_009

mv starBEAST_kyp096_009E.xml ./starBEAST_kyp096_009E/
mv starBEAST_kyp096_009.xml ./starBEAST_kyp096_009/
mv submit-many_slurm_starBEAST_kyp096_009E_beast_no_beagle.sh ./starBEAST_kyp096_009E/
mv submit-many_slurm_starBEAST_kyp096_009_beast_no_beagle.sh ./starBEAST_kyp096_009/

#move the sb_*.sl files to their respective directories
FH2=$"sb_"
for FILE in *.sl; do
    [ -f "$FILE" ] || break
    if
      [[ "${FILE}" =~ "$FH2" ]] ; then
      echo "${FILE}"
	  #make the sb_*.sl files executable
	  chmod 755 ${FILE}
	  #if the file endings is E.sl ,for the empty files, then
	  #move them to the directory for the empty files
	  if [[ "${FILE}" =~ "E.sl" ]] ; then 
	  	mv ${FILE} ./starBEAST_kyp096_009E/.
	  else
	  	mv ${FILE} ./starBEAST_kyp096_009/.
	  fi
	fi
done
	  
cd starBEAST_kyp096_009

chmod 755 *.sh
./submit-many_slurm_starBEAST_kyp096_009_beast_no_beagle.sh

cd ..
cd starBEAST_kyp096_009E/
chmod 755 *.sh
./submit-many_slurm_starBEAST_kyp096_009E_beast_no_beagle.sh
