#!/bin/bash
#SBATCH -J starBEAST_kyp095_025
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/starBEAST_kyp095_025/starBEAST_kyp095_025
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_lc_starBEAST_kyp095_025.txt
#SBATCH -e stderr_lc_starBEAST_kyp095_025.txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -trees *.species.trees stb_kyp_094_06_comb3.trees
srun logcombiner -trees -burnin 300000000 *species.trees starBEAST_kyp095_025_comb3.trees
