#!/bin/bash
#SBATCH -J nex
#SBATCH -A uoa00029         # Project Account
#SBATCH --time=48:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
####SBATCH --gres=gpu
#SBATCH --ntasks=8
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout.mb.kyp096.txt
#SBATCH -e stderr.mb.kyp096.txt

###SBATCH -C sb               # sb=Sandybridge,wm=Westmere


module load MrBayes/3.2.6-gimkl-2017a
srun mb kyp096.nex