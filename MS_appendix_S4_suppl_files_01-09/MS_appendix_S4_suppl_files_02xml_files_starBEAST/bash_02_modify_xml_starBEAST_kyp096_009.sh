#!/bin/bash
# -*- coding: utf-8 -*-

##get the present directory
WD=$(pwd)
#INPFILE="tree_to_modify.txt"
#INPFILE="xml_to_modify.xml"

FH="starBEAST_kyp096_009"
OUTDIR="out02_"${FH}"_edited_xmls"
rm -rf ${OUTDIR}
mkdir ${OUTDIR}

#echo "${FH}"
#OUTP="output.xml"
#rm ${OUTP}

#___________________________________________________________________________________________
#
# Section 01 - Edit xml files
#___________________________________________________________________________________________

STR01='<newick usingDates="false">'
STR02='</newick>'

#loop through xml files, and echo match w filehandle FH
for F in *.xml; do
    [ -f "$F" ] || break
    if
      [[ "${F}" =~ "$FH" ]] ; then

        #prepare a file that the output can be written to
      	touch ${WD}/tmp01.xml
      	iconv -f UTF-8 -t UTF-8 ${WD}/tmp01.xml
		
      	#use a while read line loop to remove all end of lines and tabs in the StartingTree
      	flag=false
      	#while read LINE
      	#Notice you need the read line function to also read white space to keep indentation
      	# see this website: https://unix.stackexchange.com/questions/131766/why-does-my-shell-script-choke-on-whitespace-or-other-special-characters
      	while IFS= read -r LINE
      	do
      	#check if STR01 is in line
      	  if [[ ${LINE} =~ .*"${STR01}".* ]] ; then
      	    flag=true
      	    #print line to output with a newline character
      	    printf "\n${LINE}\n" >> tmp01.xml
      	    #also read the next line
      	    IFS= read -r LINE1
      	    #and also print this line
      	    printf "\n${LINE1}" >> tmp01.xml
      	  elif [[ ${LINE} =~ .*"${STR02}".* ]] ; then
      	    #print a newline character to the output
      	    printf "\n" >> tmp01.xml
      	    #print line to output with a newline character
      	    printf "${LINE}" >> tmp01.xml
      	    flag=false
      	  elif [[ "${flag}" == true ]] ; then
      	    #if flag is true
      	    #notice that there is a space between the hard brackets and the variable
      	    #and likewise at the end. It cannot recognize the variable without
      	    #the leading space in front
      	    #use sed to delete end-of-lines
      	    LINE2=$(echo "${LINE}" | sed -e ':a;N;$!ba;s/\n//g' |\
      	    # and delete tabs using sed
			#use the dollar sign in front as in this example
			#as this will allow you to replace tab on MacOSX
			#https://stackoverflow.com/questions/5398395/how-can-i-insert-a-tab-character-with-sed-on-os-x
			sed -E $'s/\t//g')
      	      #sed -E 's/\t//g')
      	    printf "${LINE2}" >> tmp01.xml
      	      flag=true
      	  elif [[ "${flag}" == false ]] ; then
      	        printf "\n${LINE}" >> tmp01.xml
      	        flag=false
      	  else
      	  #  #print line to output with a newline character
      	    printf "${LINE}" >> tmp01.xml
      	  #  flag=false
      	  fi
      	done < "${F}"

      	#variables to use and replace with
      	RESCH="65.0"
      	POPSZ="100.0"
      	SPECPOPM="100.0"
      	RSCH2="100.0"
      	SPPSPLPOP="100.0"
      	CLH="65.0"
      	#edit constantSize id="spInitDemo" units="years"
      	#to
      	#<constantSize id="spInitDemo" units="years" rescaleHeight="65.0">
      	cat tmp01.xml | sed -e 's:<constantSize id="spInitDemo" units="years">:<constantSize id="spInitDemo" units="years" rescaleHeight="'${RESCH}'">:g' |\

      	# edit #	<parameter id="sp.popSize" value="1.0"/>
      	#to
      	#	<parameter id="sp.popSize" value="100.0"/>
      	sed -e 's:<parameter id="sp.popSize" value="1.0"/>:<parameter id="sp.popSize" value="'${POPSZ}'"/>:g' |\

      	#edit <parameter id="species.popMean" value="1.0" lower="0.0"/>
      	# to
      	# <parameter id="species.popMean" value="100.0" lower="0.0"/>
      	sed -e 's:<parameter id="species.popMean" value="1.0" lower="0.0"/>:<parameter id="species.popMean" value="'${SPECPOPM}'" lower="0.0"/>:g' |\

      	#edit newick usingDates
      	sed -e 's:<newick usingDates="false">:<newick usingDates="false" units="years" rescaleHeight="'${RSCH2}'">:g' |\

      	#edit <sppSplitPopulations
      	sed -e 's:<sppSplitPopulations value="1.0">:<sppSplitPopulations value="'${SPPSPLPOP}'">:g' |\

      	#edit useAmbiguities
      	sed -e 's:useAmbiguities="false":useAmbiguities="true":g' > tmp02.xml


      	#search and replace for a new text string
      	INPFILE2="tmp02.xml"
      	STR03='<!-- species starting tree for calibration'
      	STR04='<tmrca monophyletic="false">'
      	#prepare a file that the output can be written to
      	touch ${WD}/tmp03.xml
      	iconv -f UTF-8 -t UTF-8 ${WD}/tmp03.xml

      	#use a while read line loop to remove all end of lines and tabs in the StartingTree
      	flag=false
      	#while read LINE
      	#Notice you need the read line function to also read white space to keep indentation
      	# see this website: https://unix.stackexchange.com/questions/131766/why-does-my-shell-script-choke-on-whitespace-or-other-special-characters
      	while IFS= read -r LINE
      	do
      	#check if STR01 is in line
      	  if [[ ${LINE} =~ .*"${STR03}".* ]] ; then
      	    flag=true
      	    #print line to output with a newline character
      	    printf "\n${LINE}\n" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE1
      	    #and also print this line
      	    printf "\n${LINE1}" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE2
      	    #and also print this line
      	    printf "\n${LINE2}" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE3
      	    #and also print this line
      	    printf "\n${LINE3}" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE4
      	    #and also print this line
      	    printf "\n${LINE4}" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE5
      	    #and also print this line
      	    printf "\n${LINE5}" >> tmp03.xml
      	    #also read the next line
      	    IFS= read -r LINE6
      	    #and also print this line
      	    printf "\n${LINE6}" >> tmp03.xml
      	    #print the text to insert
      	    printf '\n<clade height="65.0">\n' >> tmp03.xml
      	    printf '<taxa idref="root"/>\n' >> tmp03.xml
      	    printf '</clade>' >> tmp03.xml
      	  elif [[ ${LINE} =~ .*"${STR04}".* ]] ; then
      	    #print a newline character to the output
      	    printf "\n" >> tmp03.xml
      	    #print line to output with a newline character
      	    printf "${LINE}" >> tmp03.xml
      	    flag=false
      	  elif [[ "${flag}" == false ]] ; then
      	        printf "\n${LINE}" >> tmp03.xml
      	        flag=false
      	  else
      	  #  #print line to output with a newline character
      	    printf "${LINE}" >> tmp03.xml
      	  #  flag=false
      	  fi
      	done < "${INPFILE2}"



      	#search and replace for a new text string
      	INPFILE3="tmp03.xml"
      	STR05='<!-- Species Sets'
      	STR06='<!-- Species tree prior: Yule Model'
      	#prepare a file that the output can be written to
      	touch ${WD}/tmp04.xml
      	iconv -f UTF-8 -t UTF-8 ${WD}/tmp04.xml

      	#use a while read line loop to remove all end of lines and tabs in the StartingTree
      	flag=false
      	#while read LINE
      	#Notice you need the read line function to also read white space to keep indentation
      	# see this website: https://unix.stackexchange.com/questions/131766/why-does-my-shell-script-choke-on-whitespace-or-other-special-characters
      	while IFS= read -r LINE
      	do
      	#check if STR01 is in line
      	  if [[ ${LINE} =~ .*"${STR05}".* ]] ; then
      	    flag=true
      	    #print line to output with a newline character
      	    printf "\n${LINE}\n" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE1
      	    #and also print this line
      	    printf "\n${LINE1}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE2
      	    #and also print this line
      	    printf "\n${LINE2}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE3
      	    #and also print this line
      	    printf "\n${LINE3}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE4
      	    #and also print this line
      	    printf "\n${LINE4}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE5
      	    #and also print this line
      	    printf "\n${LINE5}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE6
      	    #and also print this line
      	    printf "\n${LINE6}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE7
      	    #and also print this line
      	    printf "\n${LINE7}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE8
      	    #and also print this line
      	    printf "\n${LINE8}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE9
      	    #and also print this line
      	    printf "\n${LINE9}" >> tmp04.xml
      	    #print the text to insert
      	    printf '\n<clade height="65.0">\n' >> tmp04.xml
      	    printf '<taxa idref="root"/>\n' >> tmp04.xml
      	    printf '</clade>' >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE10
      	    #and also print this line
      	    printf "\n${LINE10}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE11
      	    #and also print this line
      	    printf "\n${LINE11}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE12
      	    #and also print this line
      	    printf "\n${LINE12}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE13
      	    #and also print this line
      	    printf "\n${LINE13}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE14
      	    #and also print this line
      	    printf "\n${LINE14}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE15
      	    #and also print this line
      	    printf "\n${LINE15}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE16
      	    #and also print this line
      	    printf "\n${LINE16}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE17
      	    #and also print this line
      	    printf "\n${LINE17}" >> tmp04.xml
      	    #also read the next line
      	    IFS= read -r LINE18
      	    #and also print this line
      	    printf "\n${LINE18}\n" >> tmp04.xml
      	    #also read the next line

      	  elif [[ ${LINE} =~ .*"${STR06}".* ]] ; then
      	    #print a newline character to the output
      	    printf "\n" >> tmp04.xml
      	    #print line to output with a newline character
      	    printf "${LINE}" >> tmp04.xml
      	    flag=false
      	  elif [[ "${flag}" == false ]] ; then
      	        printf "\n${LINE}" >> tmp04.xml
      	        flag=false
      	  else
      	  #  #print line to output with a newline character
      	    printf "${LINE}" >> tmp04.xml
      	  #  flag=false
      	  fi
      	done < "${INPFILE3}"

		#append the last line -  that for some reason went away
		#in the sections above that prepares tmp04.xml
		#BEAST will not work without this last line
		#Note ! That the lost line is not lost in tmp02.xml
		
		tail -1 "${F}" >> tmp04.xml
		#delete leading white space
		#cat tmp04.xml |\
		cat tmp02.xml |\

		#sed -e 's/^[[:space:]]*//' |\
		#delete leading end-of-lines in front the the 
		#<?xml version="1.0" standalone="yes"?>
		
		#these end-of-lines have been introduced by all the editing above
		#the xml MUST start with this exact line:
		#<?xml version="1.0" standalone="yes"?>
		#otherwise it will not start
		#see this webpage:
		#https://stackoverflow.com/questions/7359527/removing-trailing-starting-newlines-with-sed-awk-tr-and-friends
		sed -e '/./,$!d' > tmp05.xml
      	#grep -A31 '<!-- Species Sets' tmp04.xml


      	mv tmp05.xml "$WD"/"${OUTDIR}"/"${F}"
		
      	rm tmp*.xml

fi
done
#___________________________________________________________________________________________
#
#<!-- species starting tree for calibration                                   -->
#<coalescentTree id="spStartingTree">
#	<constrainedTaxa>
#		<taxa idref="allSpecies"/>
#		<tmrca monophyletic="true">
#			<taxa idref="root"/>
#
#		</tmrca>
#
#		<tmrca monophyletic="false">
#
#		to
#
#		<!-- species starting tree for calibration                                   -->
#		<coalescentTree id="spStartingTree">
#			<constrainedTaxa>
#				<taxa idref="allSpecies"/>
#				<tmrca monophyletic="true">
#					<taxa idref="root"/>
#
#				</tmrca>
#				<clade height="65.0">
#					<taxa idref="root"/>
#				</clade>
#
#				<tmrca monophyletic="false">

#___________________________________________________________________________________________
#
#		<!-- Species Sets                                                            -->
#		<tmrcaStatistic id="tmrca(root)">
#			<mrca>
#				<taxa idref="root"/>
#			</mrca>
#			<speciesTree idref="sptree"/>
#		</tmrcaStatistic>
#		<monophylyStatistic id="monophyly(root)">
#			<mrca>
#				<taxa idref="root"/>
#			</mrca>
#
# to
#
#			<!-- Species Sets                                                            -->
#			<tmrcaStatistic id="tmrca(root)">
#				<mrca>
#					<taxa idref="root"/>
#				</mrca>
#				<speciesTree idref="sptree"/>
#			</tmrcaStatistic>
#			<monophylyStatistic id="monophyly(root)">
#				<mrca>
#					<taxa idref="root"/>
#					<clade height="65.0">
#						<taxa idref="root"/>
#					</clade>
#				</mrca>
#___________________________________________________________________________________________

#___________________________________________________________________________________________
#
# Section 02 - Prepare submission scripts for slurm submission
#
#
#___________________________________________________________________________________________
# set a wall clock for the slurm submission in hours for the empty file
WLLCLK_E="48"
# set a wall clock for the slurm submission in hours for the full data file
WLLCLK="96"

#loop through xml files, and match w filehandle FH
for F in *.xml; do
    [ -f "$F" ] || break
    if
      [[ "${F}" =~ "$FH" ]] ; then
      #echo "${F}"
      FNO=$(echo "${F}" | sed 's/\.xml//g')
      #echo "${FNO}"

      #prepare a file that the output can be written to
      touch "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
      iconv -f UTF-8 -t UTF-8 "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh

    ##Notice how double quotes and dollar signs can be escaped with backslash
    # and then printed to the resulting file
    #You also need %s in front of printf to print to a previously prepared file
printf  %s "#!/bin/bash

### Job parameters

JOB_NAME="${FNO}"
INPUT_FILE=\${JOB_NAME}\".xml\"" >> "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
  #check if there is E in the filename
  #if there is, then use the wall clock limit for the empty file
  #use the dollar sign to indicate you want the E at the end of the line
  #see: https://stackoverflow.com/questions/21425006/bash-how-to-check-if-the-last-string-character-equals
    if [[ "${FNO}" =~ "E"$ ]] ; then
      printf %s "
WALL_CLOCK_LIMIT="${WLLCLK_E}":00:00" >> "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
    else
    #if there not, then use the other  wall clock limit
      printf %s "
WALL_CLOCK_LIMIT="${WLLCLK}":00:00" >> "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
    fi

printf  %s "
MEMORY=1gb
NOTIFY=ALL
ITERATIONS=8
CPUS_PER_JOB=2
### Check for input file

if [ ! -e \$INPUT_FILE ]; then
echo \"\$INPUT_FILE doesn't exist\"
fi

### Get memory and virtual memory for ulimit
mem_gb=\${MEMORY%gb}
let \"mem_kb=\${mem_gb}*1024*1024\"
let \"mem_mb=\${mem_gb}*1024\"

EXE=\"java -Xmx\${mem_gb}g -Djava.library.path=/share/apps/BEAST/noarch/1.8.4/lib -cp /share/apps/BEAST/noarch/1.8.4/lib/beast.jar dr.app.beast.BeastMain\"

EXE=\"beast -beagle_off -threads \$CPUS_PER_JOB\"
### Job creation

for (( i=0; i<\$ITERATIONS; i++ )); do

DIR=\$PWD #/\$JOB_NAME\"-0\$i\"

#if [ ! -d \$DIR ]; then
#mkdir \$DIR
#fi

FNAME=\$( echo \$INPUT_FILE | sed -e 's/.xml/-0'\$i'.xml/g' )

cp \$INPUT_FILE \$DIR/\$FNAME

sed -i 's/'\$JOB_NAME'/'\$JOB_NAME'-0'\$i'/g' \$DIR/\$FNAME

JOBFILE=\$DIR/\$JOB_NAME\"-0\$i.sl\"

echo \"#!/bin/bash\" > \$JOBFILE
echo \"#SBATCH -J \$JOB_NAME-0\$i\" >> \$JOBFILE
### echo \"#SBATCH --mail-user=sknu003@auckland.ac.nz\" >> \$JOBFILE
echo \"#SBATCH --mail-type=\$NOTIFY\" >> \$JOBFILE
echo \"#SBATCH --cpus-per-task=\$CPUS_PER_JOB\" >> \$JOBFILE
echo \"#SBATCH --ntasks=1\" >> \$JOBFILE
echo \"#SBATCH -A uoa00029\" >> \$JOBFILE
echo \"#SBATCH  --partition=long\" >> \$JOBFILE
echo \"#SBATCH --time=\$WALL_CLOCK_LIMIT\" >> \$JOBFILE
echo \"#SBATCH --mem-per-cpu=\${mem_mb}\" >> \$JOBFILE
echo \"#SBATCH -D \$DIR\" >> \$JOBFILE

echo \"#SBATCH -o \$DIR/%j.%N.out\" >> \$JOBFILE
echo \"#SBATCH -e \$DIR/%J.%N.err\" >> \$JOBFILE

### echo \"module load BEAST/1.8.2-goolf-1.5.14\" >> \$JOBFILE
echo \"module load BEAST/1.8.4-gimkl-2017a-no-beagle\"  >> \$JOBFILE
echo \"srun \$EXE \$FNAME\" >> \$JOBFILE

sbatch \$JOBFILE

done" >> "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
	#delete leading end-of-lines to ensure the very first line start with bin bash
	cat "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh | sed -e '/./,$!d' > "$WD"/"${OUTDIR}"/submit-many_slurm_"${FNO}"_beast_no_beagle.sh
	#remove the tmp file
	rm "$WD"/"${OUTDIR}"/tmp01_"${FNO}"_beast_no_beagle.sh
fi
done

#___________________________________________________________________________________________
#
# Section 03 - Prepare logcombiner slurm submission scripts
#             - and prepare treecombiner slurm submission scripts
#             - and prepare treeannotator slurm submission scripts
#___________________________________________________________________________________________

#loop through xml files, and echo match w filehandle FH
for F in *.xml; do
    [ -f "$F" ] || break
    if
      [[ "${F}" =~ "$FH" ]] ; then
      #echo "${F}"
      FNO=$(echo "${F}" | sed 's/\.xml//g')
      #echo "${FNO}"
      #get the file number without the E at the end the dollar sign tells
      #sed only to replace at the end of the line
      FNO_NoE=$(echo "${FNO}" | sed 's/E$//g')

#_______________________________make a logc combiner file___________________________
      #prepare a file that the output can be written to
      touch "$WD"/"${OUTDIR}"/tmp01_logc_"${FNO}".sl
      iconv -f UTF-8 -t UTF-8 "$WD"/"${OUTDIR}"/tmp01_logc_"${FNO}".sl

#Print the logcombiner slurm submission script file
    printf %s "#!/bin/bash
#SBATCH -J "${FNO}"
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/"${FNO_NoE}"/"${FNO}"
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_logc_"${FNO}".txt
#SBATCH -e stderr_logc_"${FNO}".txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## module load logcombiner
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner *.log stb_kyp_094_06E_comb.log
#srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 20000000 *.log stb_kyp_094_06E_comb.log
#srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 6000000 *.log btobis_15_08_86_GTR_comb.log
srun logcombiner -burnin 300000000 *.log "${FNO}"_comb.log" >> "$WD"/"${OUTDIR}"/tmp01_logc_"${FNO}".sl

#delete leading end-of-lines to ensure the very first line start with bin bash
cat "$WD"/"${OUTDIR}"/tmp01_logc_"${FNO}".sl | sed -e '/./,$!d' > "$WD"/"${OUTDIR}"/sb_logc_"${FNO}".sl
#remove the tmp file
rm "$WD"/"${OUTDIR}"/tmp01_logc_"${FNO}".sl

#_______________________________make a lc combiner file___________________________
#prepare a file that the output can be written to
touch "$WD"/"${OUTDIR}"/tmp01_lc_"${FNO}".sl
iconv -f UTF-8 -t UTF-8 "$WD"/"${OUTDIR}"/tmp01_lc_"${FNO}".sl

#Print the treecombiner slurm submission script file
printf "#!/bin/bash
#SBATCH -J "${FNO}"
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/"${FNO_NoE}"/"${FNO}"
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_lc_"${FNO}".txt
#SBATCH -e stderr_lc_"${FNO}".txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -trees *.species.trees stb_kyp_094_06_comb3.trees
srun logcombiner -trees -burnin 300000000 *species.trees "${FNO}"_comb3.trees" >> "$WD"/"${OUTDIR}"/tmp01_lc_"${FNO}".sl

#delete leading end-of-lines to ensure the very first line start with bin bash
cat "$WD"/"${OUTDIR}"/tmp01_lc_"${FNO}".sl | sed -e '/./,$!d' > "$WD"/"${OUTDIR}"/sb_lc_"${FNO}".sl
#remove the tmp file
rm "$WD"/"${OUTDIR}"/tmp01_lc_"${FNO}".sl

#_______________________________make a ta combiner file___________________________
#prepare a file that the output can be written to
touch "$WD"/"${OUTDIR}"/tmp01_ta_"${FNO}".sl
iconv -f UTF-8 -t UTF-8 "$WD"/"${OUTDIR}"/tmp01_ta_"${FNO}".sl

#Print the treecombiner slurm submission script file

printf "#!/bin/bash
#SBATCH -J "${FNO}"
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/"${FNO_NoE}"/"${FNO}"
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_ta_"${FNO}".txt
#SBATCH -e stderr_ta_"${FNO}".txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## module load logcombiner

###module load java
###srun java -Xms64m -Xmx4000m -Djava.library.path=/share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/  -cp /share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/beast.jar dr.app.tools.TreeAnnotator -heights mean stb_kyp_094_06_comb3.trees stb_kyp_094_06_mean_tree.tre
srun treeannotator -heights mean "${FNO}"_comb3.trees "${FNO}".comb.tre" >> "$WD"/"${OUTDIR}"/tmp01_ta_"${FNO}".sl

#delete leading end-of-lines to ensure the very first line start with bin bash
cat "$WD"/"${OUTDIR}"/tmp01_ta_"${FNO}".sl | sed -e '/./,$!d' > "$WD"/"${OUTDIR}"/sb_ta_"${FNO}".sl
#remove the tmp file
rm "$WD"/"${OUTDIR}"/tmp01_ta_"${FNO}".sl

#echo $FNO

fi
done

#___________________________________________________________________________________________
#
# Section 04 - Prepare a bash submission scripts to use on the remote cluster
#___________________________________________________________________________________________
#prepare a file that the output can be written to
touch "$WD"/"${OUTDIR}"/bash03_rearrange_xml_files_"${FH}".sh
iconv -f UTF-8 -t UTF-8 "$WD"/"${OUTDIR}"/bash03_rearrange_xml_files_"${FH}".sh

#notice how the %s in front of printf and backslashes are used for double quotes and 
#dollarsigns that are intended to be printed as dollar signs and double quotes
printf %s "#!/bin/bash
# -*- coding: utf-8 -*-

##get the present directory
WD=\$(pwd)

##BSTR_N=\"14\"
mkdir "${FH}"E
mkdir "${FH}"

mv "${FH}"E.xml ./"${FH}"E/
mv "${FH}".xml ./"${FH}"/
mv submit-many_slurm_"${FH}"E_beast_no_beagle.sh ./"${FH}"E/
mv submit-many_slurm_"${FH}"_beast_no_beagle.sh ./"${FH}"/

#move the sb_*.sl files to their respective directories
FH2=\$\"sb_\"
for FILE in *.sl; do
    [ -f \"\$FILE\" ] || break
    if
      [[ \"\${FILE}\" =~ \"\$FH2\" ]] ; then
      echo \"\${FILE}\"
	  #make the sb_*.sl files executable
	  chmod 755 \${FILE}
	  #if the file endings is E.sl ,for the empty files, then
	  #move them to the directory for the empty files
	  if [[ \"\${FILE}\" =~ \"E.sl\" ]] ; then 
	  	mv \${FILE} ./"${FH}"E/.
	  else
	  	mv \${FILE} ./"${FH}"/.
	  fi
	fi
done
	  
cd "${FH}"

chmod 755 *.sh
./submit-many_slurm_"${FH}"_beast_no_beagle.sh

cd ..
cd "${FH}"E/
chmod 755 *.sh
./submit-many_slurm_"${FH}"E_beast_no_beagle.sh" |\
	#delete leading end-of-lines
	 sed -e '/./,$!d' >> "$WD"/"${OUTDIR}"/bash03_rearrange_xml_files_"${FH}".sh



#make the bash03_rearrange_xml_files executable
chmod 755 "$WD"/"${OUTDIR}"/bash03_rearrange_xml_files_"${FH}".sh

#___________________________________________________________________________________________
#
# Section 05 - change to the output directory, 
#				compress the prepared files, and move the compressed tar.gz file
#___________________________________________________________________________________________

cd "${WD}"/"${OUTDIR}"/

#remove any eventual previous tar.gz file
rm "${WD}"/"${FH}".tar.gz
#compress all files
tar -zcvf "${FH}".tar.gz *

mv "${FH}".tar.gz "${WD}"/"${FH}".tar.gz
#chnage dir back to the working dir
cd "${WD}"
ORIG_XML_F_DIR=$(echo orig_unedit_xml_files_"$FH")
#delete any eventual previous version
rm -rf "${ORIG_XML_F_DIR}"
#make a new directory
mkdir -p "${ORIG_XML_F_DIR}"

#move the original xml-files
#loop through xml files, and echo match w filehandle FH
for F in *.xml; do
    [ -f "$F" ] || break
    if
      [[ "${F}" =~ "$FH" ]] ; then
	  cp "${F}" "${WD}"/"${ORIG_XML_F_DIR}"/"${F}"
  	fi
done
#

#cd $WD/$OUTDIR/
#ls -lh
#cat sb_logc_StarBEAST_kyp095_15.sl