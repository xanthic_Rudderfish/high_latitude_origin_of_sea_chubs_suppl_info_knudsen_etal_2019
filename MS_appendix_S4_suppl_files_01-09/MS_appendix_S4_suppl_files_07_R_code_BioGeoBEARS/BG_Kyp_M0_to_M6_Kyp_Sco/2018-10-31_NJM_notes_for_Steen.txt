
2018-10-31_NJM_notes_for_Steen:

Hi Steen!  It was actually faster for me to just set up some example files for you, as I had to fix a graphing bug anyway. And easier than setting up a call.

In the attached:

M0: a run of the six basic models, no modifiers

M3: a run of the six basic models, with time-stratification so that East Pacific-Caribbean dispersal is blocked at 3 mya (note that the lnL improves!). 

I also put in a stratum at 25 mya, in case you want to do something with the Tethys (by changing 1s to 0s), but as Indian-to-Atlantic dispersal around South Africa may be happening anyway, it may not be worth it. Anyway, you can modify as you like

M4: M3, plus your manual modification of the list of allowed ranges (I also disallowed these, as they seemed unlikely: 
"EO", "AS", "OS", "ES"

As changing the list of allowed ranges "changes the data" in a sense, the lnLs from M4 cannot be statistically compared to the M0/M3 runs, because likelihoods/AICs can only be compared between different models on identical data (but, you can compare by eye and make a judgement about whether the more-limited-states-list gives a more reasonable result.)

M5: M4, plus I've put a multiplier of 0 for dispersal between non-adjacent areas, and a multiplier of 0.1 if the dispersal would be eastwards against the current. The actual dispersal multiplier will be 0.1^w, with w estimated as a free parameter.

M6: M4, plus your distances matrix, scaled to be a relative distances matrix. This is taken to the power x, where x is a free parameter that is estimated.

I think you can just modify these more easily than I can explain things!  As model comparison isn't your main goal and 16 species isn't much data to compare lots of model or estimate lots of parameters, I would just pick the group of 6 that has a decent prior justification, and then use the best model therein.


Also: I improved the graphics a bit so it is more readable.

Summary: Having several widespread species, and a small phylogeny, means there isn't a lot of signal, and the modifications you make to the states list and dispersal modifers can matter a lot.

While we tend to look at the "single most probable range" plots, it is more scientific to focus on the pie charts (http://phylo.wikidot.com/biogeobears-mistakes-to-avoid#Most_prob_states_not_joint ). 

Main message of the pie charts: high uncertainty if you go deep in the tree. This is expected if some of the species are widespread and others are regional endemics, this suggests ranges change quite a bit in the phylogeny, so ancestral estimates will be uncertain.

Cheers!
Nick

