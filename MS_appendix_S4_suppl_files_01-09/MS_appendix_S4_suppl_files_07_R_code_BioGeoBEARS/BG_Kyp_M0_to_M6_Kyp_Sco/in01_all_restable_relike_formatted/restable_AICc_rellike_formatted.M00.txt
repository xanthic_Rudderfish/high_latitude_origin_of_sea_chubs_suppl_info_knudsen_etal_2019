LnL	numparams	d	e	j	AICc	AICc_wt
DEC	-58.32	2	0.049	0.080	0	121.6	0.0049
DEC+J	-58.32	3	0.049	0.080	1.0e-05	124.8	0.0010
DIVALIKE	-60.31	2	0.046	0.073	0	125.6	0.0007
DIVALIKE+J	-60.01	3	0.061	0.098	1.0e-05	128.2	0.0002
BAYAREALIKE	-56.37	2	0.045	0.13	0	117.7	0.035
BAYAREALIKE+J	-51.46	3	0.013	0.033	0.16	111.1	0.96
