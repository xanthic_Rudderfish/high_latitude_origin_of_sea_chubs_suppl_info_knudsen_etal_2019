alt;null;LnLalt;LnLnull;DFalt;DFnull;DF;Dstatistic;pval;test;tail;AIC1;AIC2;AICwt1;AICwt2;AICweight_ratio_model1;AICweight_ratio_model2;modelname
1;DEC+J;DEC;-62.95;-64.62;3;2;1;3.33;0.068;chi-squared;one-tailed;131.9;133.2;0.66;0.34;1.95;0.51;M05
2;DIVALIKE+J;DIVALIKE;-66.78;-72.29;3;2;1;11.03;0.0009;chi-squared;one-tailed;139.6;148.6;0.99;0.011;91.42;0.011;M05
3;BAYAREALIKE+J;BAYAREALIKE;-60.79;-60.82;3;2;1;0.057;0.81;chi-squared;one-tailed;127.6;125.6;0.27;0.73;0.38;2.64;M05
