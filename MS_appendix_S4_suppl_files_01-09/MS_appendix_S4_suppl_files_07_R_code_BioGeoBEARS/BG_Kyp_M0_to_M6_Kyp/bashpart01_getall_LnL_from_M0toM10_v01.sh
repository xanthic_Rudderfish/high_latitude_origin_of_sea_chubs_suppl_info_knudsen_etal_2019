#!/bin/bash

WD=$(pwd)
#make a new directory for the results from this script
OUTDIR="out01_all_restable_relike_formatted"
rm -rf $OUTDIR
mkdir -p $OUTDIR
#make a new directory for the input files for this script
INDIR="in01_all_restable_relike_formatted"
rm -rf $INDIR
mkdir -p $INDIR

#____________________________________________________________________________
# Traverse all subdirectories and get specified files
#
#____________________________________________________________________________
#Se how to traverse directories and subdirectories here:
#https://unix.stackexchange.com/questions/187167/traverse-all-subdirectories-in-and-do-something-in-unix-shell-script
for D in $(find /${WD}/M*/ -maxdepth 1 -type d)
do
  #Do something, the directory is accessible with $D:
  #echo $D
  #get only the subdirectory folder name
  #https://askubuntu.com/questions/76808/how-do-i-use-variables-in-a-sed-command
  #and replace forwardslashes
  MN=$(echo $D | sed "s,${WD},,g" | sed 's:/::g')
  #this will give you the model name
  #echo "${MN}"
  #cd $D
  #copy the files from each subdirectory to the inputdirectory
  cp "${D}"/restable_AICc_rellike_formatted.txt "${WD}"/"${INDIR}"/restable_AICc_rellike_formatted."${MN}".txt
  cp "${D}"/teststable.txt "${WD}"/"${INDIR}"/teststable."${MN}".txt
  cp "${D}"/restable_AIC_rellike_formatted.txt "${WD}"/"${INDIR}"/restable_AIC_rellike_formatted."${MN}".txt
done #>output_file
#ls -l "${WD}"/"${OUTDIR}"/

#____________________________________________________________________________
# Add the model name to the end of each line in the AICc restable files
#
#____________________________________________________________________________
#change diretory to where all AICc files are copied to
cd "${WD}"/"${INDIR}"/
#loop over files
for FILE in restable_AICc_rellike_formatted.*.txt
do
  #echo "${FILE}"
  #get model name
  MN=$(echo "${FILE}" | awk -F  "." '{print $2}')
  #make a new empty file for each model
  touch nw01_tableAICc_"${MN}".txt
  iconv -f UTF-8 -t UTF-8 nw01_tableAICc_"${MN}".txt
  #for each line in the input file
  while read line
  do
    if [[ $line == "LnL"* ]] # if line starts with 'LnL'
      then
        # add to each end of each line
        echo ${line:0} |\
        sed -e "s,$,___modelname,g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_tableAICc_"${MN}".txt
      else # if line does not start with 'LnL' then
        echo ${line:0} |\
        #add the model name to the end of the line
        sed -e "s,$,___${MN},g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_tableAICc_"${MN}".txt
    fi
  done < "${FILE}"
  #head -3 nw_tableAICc_"${MN}".txt
done

#____________________________________________________________________________
# Add the model name to the end of each line in the AIC restable files
# NOTICE ! The difference between AICc and AIC !!
#____________________________________________________________________________
#change diretory to where all AIC files are copied to
cd "${WD}"/"${INDIR}"/
#loop over files
for FILE in restable_AIC_rellike_formatted.*.txt
do
  #echo "${FILE}"
  #get model name
  MN=$(echo "${FILE}" | awk -F  "." '{print $2}')
  #make a new empty file for each model
  touch nw01_tableAIC_"${MN}".txt
  iconv -f UTF-8 -t UTF-8 nw01_tableAIC_"${MN}".txt
  #for each line in the input file
  while read line
  do
    if [[ $line == "LnL"* ]] # if line starts with 'LnL'
      then
        # add to each end of each line
        echo ${line:0} |\
        sed -e "s,$,___modelname,g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_tableAIC_"${MN}".txt
      else # if line does not start with 'LnL' then
        echo ${line:0} |\
        #add the model name to the end of the line
        sed -e "s,$,___${MN},g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_tableAIC_"${MN}".txt
    fi
  done < "${FILE}"
  #head -3 nw_tableAIC_"${MN}".txt
done


#____________________________________________________________________________
# Get only the LnL columns from the AICc restable files
#
#____________________________________________________________________________

for FILE in nw01_tableAICc_*.txt
do
  MN=$(echo "${FILE}" | awk -F  "." '{print $1}' | sed 's/nw01_tableAICc_//g')
  #echo ${MN}
  #make a new empty file for each model
  touch nw02_tableAICc_"${MN}".txt
  iconv -f UTF-8 -t UTF-8 nw02_tableAICc_"${MN}".txt
  #for each line in the input file
  while read line
    do
      if [[ $line == "LnL"* ]] # if line starts with 'LnL'
        then
          # add to each end of each line
          echo ${MN}'_LnL' >> nw02_tableAICc_"${MN}".txt
        else # if line does not start with 'LnL' then
          echo ${line:0} |\
          #replace tabs w semicolons
          awk -F  ";" '{print $2}' >> nw02_tableAICc_"${MN}".txt
      fi
    done < "${FILE}"
  #head -4 nw02_tableAICc_"${MN}".txt
done

#____________________________________________________________________________
# Get only the LnL columns from the AIC restable files
#
#____________________________________________________________________________

for FILE in nw01_tableAIC_*.txt
do
  MN=$(echo "${FILE}" | awk -F  "." '{print $1}' | sed 's/nw01_tableAIC_//g')
  #echo ${MN}
  #make a new empty file for each model
  touch nw02_tableAIC_"${MN}".txt
  iconv -f UTF-8 -t UTF-8 nw02_tableAIC_"${MN}".txt
  #for each line in the input file
  while read line
    do
      if [[ $line == "LnL"* ]] # if line starts with 'LnL'
        then
          # add to each end of each line
          echo ${MN}'_LnL' >> nw02_tableAIC_"${MN}".txt
        else # if line does not start with 'LnL' then
          echo ${line:0} |\
          #replace tabs w semicolons
          awk -F  ";" '{print $2}' >> nw02_tableAIC_"${MN}".txt
      fi
    done < "${FILE}"
  #head -4 nw02_tableAIC_"${MN}".txt
done


#paste all nw02-files together
paste nw02_tableAICc_*.txt > allAICc_tableLnL01.txt
paste nw02_tableAIC_*.txt > allAIC_tableLnL01.txt

#____________________________________________________________________________
# Get the first column from the AICc rand AIC estable files
#
#____________________________________________________________________________

#loop over all nw01 files
for FILE in nw01_tableAIC*_*.txt
do
  MN=$(echo "${FILE}" | awk -F  "." '{print $1}' | sed 's/nw01_table//g')
  #echo $FILE
  #echo $MN
  #cut the first field using semicolon as delimiter
  cut -d ";" -f1 $FILE > tmp01.${MN}.txt
done

#____________________________________________________________________________
# paste all LnL columns togther in a file
#
#____________________________________________________________________________

#paste the two tmp files together
paste tmp01.AICc_*.txt > tmp02.AICc.txt
paste tmp01.AIC_*.txt > tmp02.AIC.txt
#cut the first field
cut -f1 tmp02.AICc.txt > tmp03.AICc.txt
cut -f1 tmp02.AIC.txt > tmp03.AIC.txt
#paste two files together
paste tmp03.AIC.txt allAIC_tableLnL01.txt > allAIC_tableLnL02.txt
paste tmp03.AICc.txt allAICc_tableLnL01.txt > allAICc_tableLnL02.txt
#copy the resulting file
cp allAIC_tableLnL02.txt ${WD}/${OUTDIR}/
cp allAICc_tableLnL02.txt ${WD}/${OUTDIR}/


#____________________________________________________________________________
# Add the model name to the end of each line in the teststable.txt files
#
#____________________________________________________________________________
#change diretory to where all AICc files are copied to
cd "${WD}"/"${INDIR}"/
#loop over files
for FILE in teststable.*.txt
do
  #get model name
  MN=$(echo "${FILE}" | awk -F  "." '{print $2}')
  #make a new empty file for each model
  touch nw01_teststable_"${MN}".txt
  iconv -f UTF-8 -t UTF-8 nw01_teststable_"${MN}".txt
  #for each line in the input file
  while read line
  do
    if [[ $line == "alt"* ]] # if line starts with 'alt'
      then
        # add to each end of each line
        echo ${line:0} |\
        sed -e "s,$,___modelname,g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_teststable_"${MN}".txt
      else # if line does not start with 'alt' then
        echo ${line:0} |\
        #add the model name to the end of the line
        sed -e "s,$,___${MN},g" |\
        #replace the mutliple underscores with tabs
        sed 's/___/\t/' |\
        #replace all spaces with tabs
        awk -v OFS="\t" '$1=$1' |\
        #replace tabs w semicolons
        sed 's/\t/;/g' >> nw01_teststable_"${MN}".txt
    fi
  done < "${FILE}"
done

#see note about quotes around variables: https://stackoverflow.com/questions/2462385/getting-an-ambiguous-redirect-error
# especially for writing to a file in a path that incl. spaces
for FILE in nw01_teststable_*.txt
do
	#write the first line of every csv-file into a temporary file
	head -1 ${FILE} | sed 's/^alt;/alt1;alt2;/g' >> tmp04.txt
done

#get the unique lines from the tmp04.txt file,
#if all csv-files are set up in the same way, this should return only a single line
#this line can put into the outputfile and serve as a header with column names
cat tmp04.txt | uniq > all_nw02_teststable.txt

#see this website on how to use sed to get all lines apart from the first line:
#https://unix.stackexchange.com/questions/55755/print-file-content-without-the-first-and-last-lines/55757
for FILE in nw01_teststable_*.txt
do
	sed '1d' ${FILE} >> all_nw02_teststable.txt
done

# see this website about : Echo newline in Bash prints literal \n
# https://stackoverflow.com/questions/8467424/echo-newline-in-bash-prints-literal-n
# -e flag did it for me, which "enables interpretation of backslash escapes"

echo -e " \n make sure there only is one unique line for headers \n"

#see the content of the tmp01.txt file, to check all input files have the same header
#using the uniq command in the end , will make sure it only returns the unique lines
cat tmp04.txt | uniq

#set input field seperator and outfield seperator, and print selected columns
#with awk. The columns have been selected by first inspecting all columns,
#to see which columns are non-variable across the entire column
awk 'BEGIN {FS=";"; OFS=";"} ; {print $2, $3, $4, $5, $9, $10, $13, $14, $15, $16, $17, $18, $19}' all_nw02_teststable.txt |\
#add a column the specifies the constraints and the abbreviations used for the constraints

#modify the last column, so 2 new columns are added with details about constraints
sed -e "s,modelname$,modelname;constraint;Abbreviation_constraint,g" |\
sed -e "s,M00$,M00;na;na,g" |\
sed -e "s,M01$,M01;timeperiods_and_time_restricted_adjacent_regions_and_time_stratified_adjacent_regions_and_time_stratified_oceanic_barriers;tp_and_trar_and_tsar_and_tsob,g" |\
sed -e "s,M02$,M02;timeperiods_and_time_restricted_adjacent_regions;tp_and_trar,g" |\
sed -e "s,M03$,M03;timeperiods_and_time_stratified_dispersal;tp_and_tsd,g" |\
sed -e "s,M04$,M04;timeperiods_and_time_stratified_dispersal;tp_and_tsd,g" |\
sed -e "s,M05$,M05;timeperiods_and_time_restricted_adjacent_regions_and_time_stratified_connectivity_westward_current;tp_and_trar_and_tscwwc,g" |\
sed -e "s,M06$,M06;timeperiods_and_time_stratified_dispersal_and_distance_matrix;tp_and_tsd_and_dm,g" |\
sed -e "s,M07$,M07;timeperiods_and_time_stratified_dispersal_and_time_restricted_adjacent_regions;tp_and_tsd_and_trar,g" |\
sed -e "s,M08$,M08;climatic_zones;cz,g" > all_nw03_teststable.txt
#move the resulting file
mv all_nw03_teststable.txt ${WD}/${OUTDIR}/all_nw03_teststable.txt

#change dir to the outdir
cd ${WD}/${OUTDIR}/
#_______________________________________________________________________________
#transpose the AIC and AICc data matrix
#_______________________________________________________________________________
# see this website: https://stackoverflow.com/questions/1729824/an-efficient-way-to-transpose-a-file-in-bash
# and this website : https://www.thelinuxrain.com/articles/transposing-rows-and-columns-3-methods
for FILE in all*_tableLnL02.txt
do
A=$(echo "${FILE}" | sed 's/all//g' | sed 's/_tableLnL02.txt//g')
awk '
{
   for (i=1; i<=NF; i++)  {
       a[NR,i] = $i
   }
}
NF>p { p = NF }
END {
   for(j=1; j<=p; j++) {
       str=a[1,j]
       for(i=2; i<=NR; i++){
           str=str" "a[i,j];
       }
       print str
   }
}' ${FILE} > all"${A}"_tableLnL03.txt
done

#_______________________________________________________________
# Rearrange the all*_tableLnL03.txt files to make a files
# for each BioGeoBEARS model
#_______________________________________________________________
for FILE in all*_tableLnL03.txt
do
  A=$(echo "${FILE}" | sed 's/all//g' | sed 's/_tableLnL03.txt//g')
  #make a new empty file for each model
  touch tmptable_DEC."${A}".txt
  iconv -f UTF-8 -t UTF-8 tmptable_DEC."${A}".txt
  #make a new empty file for each model
  touch tmptable_DIVA."${A}".txt
  iconv -f UTF-8 -t UTF-8 tmptable_DIVA."${A}".txt
  #make a new empty file for each model
  touch tmptable_BAYAREALIKE."${A}".txt
  iconv -f UTF-8 -t UTF-8 tmptable_BAYAREALIKE."${A}".txt
  while read line
    do
      if [[ $line == "LnL"* ]] # if line starts with 'LnL'
        then
          # cut fields in header line
          echo ${line:0} |\
          # cut fields in header line
          cut -d " " -f 1-3 |\
          #append the AIC or AICc to the column headers
          #also add BGBmodel and a sort no header for a later sort
          #see this website: https://stackoverflow.com/questions/18061606/add-string-in-each-line-at-the-begining-of-column
          awk 'BEGIN{OFS="\t"}$2=$2"_'${A}'",$3=$3"_'${A}'\tBGBmodel\tsortno"' |\
          sed 's/DEC/BGBmodelnm/g' >> tmptable_DEC."${A}".txt
          echo ${line:0} |\
          cut -d " " -f 1,4-5 |\
          #append the AIC or AICc to the column headers
          awk 'BEGIN{OFS="\t"}$2=$2"_'${A}'",$3=$3"_'${A}'\tBGBmodel\tsortno"' |\
          sed 's/DIVALIKE/BGBmodelnm/g' >> tmptable_DIVA."${A}".txt
          echo ${line:0} |\
          cut -d " " -f 1,6-7 |\
          #append the AIC or AICc to the column headers
          awk 'BEGIN{OFS="\t"}$2=$2"_'${A}'",$3=$3"_'${A}'\tBGBmodel\tsortno"' |\
          sed 's/BAYAREALIKE/BGBmodelnm/g' >> tmptable_BAYAREALIKE."${A}".txt
        else # if line does not start with 'LnL' then
          #cut and append to files above
          echo ${line:0} |\
          cut -d " " -f 1-3 |\
          sed -e "s,$,\tDEC\t01,g" >> tmptable_DEC."${A}".txt
          echo ${line:0} |\
          cut -d " " -f 1,4-5 |\
          sed -e "s,$,\tDIVA\t02,g" >> tmptable_DIVA."${A}".txt
          echo ${line:0} |\
          cut -d " " -f 1,6-7 |\
          sed -e "s,$,\tBAYAREALIKE\t03,g" >> tmptable_BAYAREALIKE."${A}".txt
      fi
    done < "${FILE}"
done

#combine all table_BGBmodel files
paste tmptable_DEC.*.txt > tmptable_DEC_02.txt
paste tmptable_DIVA.*.txt > tmptable_DIVA_02.txt
paste tmptable_BAYAREALIKE.*.txt > tmptable_BAYAREALIKE_02.txt

#for all table_BGBmodel files
for FILE in tmptable_*_02.txt
do
  A=$(echo "${FILE}" | sed 's/tmptable_//g' | sed 's/_02.txt//g')
  #echo $A
  while read line
    do
      if [[ $line == "LnL"* ]] # if line starts with 'LnL'
        then
          # cut fields in header line
          echo ${line:0} >> tmp05.txt

        else
          echo ${line:0} >> tmp06.txt
        fi
      done < "${FILE}"
done

#get the unique header
cat tmp05.txt | uniq > tmp07.header.txt
#sort the lines in tmp06.txt file
#To sort by a single column, use -k2,2 as the key specification. This means to use the fields from #2 to #2, i.e. only the second field.
sort -k1,1 -k10,10 tmp06.txt > tmp08.txt
#assemble the two last tmp files
cat tmp07.header.txt tmp08.txt > tmp09.txt
#cut out the columns needed
cut -d" " -f2-3,7-8 tmp09.txt |\
#replace all speces w tabs
awk -v OFS="\t" '$1=$1' |\
sed 's/\t/;/g' > tmp10.txt
#paste the two files together
paste all_nw03_teststable.txt tmp10.txt |\
#delete lines with some of the models- the models are here preselected
awk '!/M01/' |\
awk '!/M04/' |\
awk '!/M06/' |\
#replace all spaces w tabs
awk -v OFS="\t" '$1=$1' |\
sed 's/\t/;/g' > all_nw04_teststable.txt
#use sed to replace all semicolons w tabs
sed 's/;/\t/g' all_nw04_teststable.txt > all_teststable_all_AIC.txt

#remove some tmp files
rm tmp*

#after inspecting the resulting files
echo "fantastic piece of code. I Just found out the columns I ended up appending are already present in the first table. There was no need to extract all the information from the AIC and AICc tables. It was already available in the first table. At least I learned some more bash"
#see !!
cut -f3-4,16-19 all_teststable_all_AIC.txt

#
