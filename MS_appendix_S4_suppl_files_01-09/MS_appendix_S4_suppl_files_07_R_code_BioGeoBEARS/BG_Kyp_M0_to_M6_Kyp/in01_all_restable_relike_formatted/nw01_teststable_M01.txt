alt;null;LnLalt;LnLnull;DFalt;DFnull;DF;Dstatistic;pval;test;tail;AIC1;AIC2;AICwt1;AICwt2;AICweight_ratio_model1;AICweight_ratio_model2;modelname
1;DEC+J;DEC;-44.69;-47.73;3;2;1;6.09;0.014;chi-squared;one-tailed;95.38;99.47;0.89;0.11;7.71;0.13;M01
2;DIVALIKE+J;DIVALIKE;-46.64;-48.25;3;2;1;3.22;0.073;chi-squared;one-tailed;99.27;100.5;0.65;0.35;1.84;0.54;M01
3;BAYAREALIKE+J;BAYAREALIKE;-39.09;-46.52;3;2;1;14.86;0.0001;chi-squared;one-tailed;84.17;97.03;1.00;0.0016;619.3;0.0016;M01
