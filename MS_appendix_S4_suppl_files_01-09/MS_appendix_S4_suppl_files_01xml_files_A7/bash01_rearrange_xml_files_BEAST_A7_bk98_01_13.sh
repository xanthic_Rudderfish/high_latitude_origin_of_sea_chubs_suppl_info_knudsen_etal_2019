#!/bin/bash
# -*- coding: utf-8 -*-

##get the present directory
WD=$(pwd)

##BEAST_A7_bk98_01_13
BSTR_N="13"
mkdir BEAST_A7_bk98_01_"${BSTR_N}"E
mkdir BEAST_A7_bk98_01_"${BSTR_N}"
mv BEAST_A7_bk98_01_"${BSTR_N}"E.xml ./BEAST_A7_bk98_01_"${BSTR_N}"E/
mv BEAST_A7_bk98_01_"${BSTR_N}".xml ./BEAST_A7_bk98_01_"${BSTR_N}"/
mv submit-many_slurm_BEAST_A7_bk98_01_"${BSTR_N}"E.no_beagle.sh ./BEAST_A7_bk98_01_"${BSTR_N}"E/
mv submit-many_slurm_BEAST_A7_bk98_01_"${BSTR_N}".no_beagle.sh ./BEAST_A7_bk98_01_"${BSTR_N}"/

cd BEAST_A7_bk98_01_"${BSTR_N}"

chmod 755 *
./submit-many_slurm_BEAST_A7_bk98_01_"${BSTR_N}".no_beagle.sh 

cd ..
cd BEAST_A7_bk98_01_"${BSTR_N}"E/
chmod 755 *
./submit-many_slurm_BEAST_A7_bk98_01_"${BSTR_N}"E.no_beagle.sh 
