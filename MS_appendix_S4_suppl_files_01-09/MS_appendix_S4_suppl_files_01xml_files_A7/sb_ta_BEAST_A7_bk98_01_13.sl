#!/bin/bash
#SBATCH -J BEAST_A7_bk98_01_13
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /home/sknu003/pan_home/kyp_project/pan_project/BEAST_A7_bk98_01_13/BEAST_A7_bk98_01_13
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_ta_BEAST_A7_bk98_01_13.txt
#SBATCH -e stderr_ta_BEAST_A7_bk98_01_13.txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## module load logcombiner

###module load java
###srun java -Xms64m -Xmx4000m -Djava.library.path=/share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/  -cp /share/easybuild/RHEL6.3/sandybridge/software/BEAST/1.8.0-goolf-1.5.14/lib/beast.jar dr.app.tools.TreeAnnotator -heights mean stb_kyp_094_06_comb3.trees stb_kyp_094_06_mean_tree.tre
srun treeannotator -heights mean BEAST_A7_bk98_01_13_comb3.trees