#!/bin/bash
# -*- coding: utf-8 -*-

##get the present directory
WD=$(pwd)

##BSTR_N="14"
mkdir BEAST_A7_bk98_01_13E
mkdir BEAST_A7_bk98_01_13

mv BEAST_A7_bk98_01_13E.xml ./BEAST_A7_bk98_01_13E/
mv BEAST_A7_bk98_01_13.xml ./BEAST_A7_bk98_01_13/
mv submit-many_slurm_BEAST_A7_bk98_01_13E_beast_no_beagle.sh ./BEAST_A7_bk98_01_13E/
mv submit-many_slurm_BEAST_A7_bk98_01_13_beast_no_beagle.sh ./BEAST_A7_bk98_01_13/

#move the sb_*.sl files to their respective directories
FH2=$"sb_"
for FILE in *.sl; do
    [ -f "$FILE" ] || break
    if
      [[ "${FILE}" =~ "$FH2" ]] ; then
      echo "${FILE}"
	  #make the sb_*.sl files executable
	  chmod 755 ${FILE}
	  #if the file endings is E.sl ,for the empty files, then
	  #move them to the directory for the empty files
	  if [[ "${FILE}" =~ "E.sl" ]] ; then 
	  	mv ${FILE} ./BEAST_A7_bk98_01_13E/.
	  else
	  	mv ${FILE} ./BEAST_A7_bk98_01_13/.
	  fi
	fi
done
	  
cd BEAST_A7_bk98_01_13

chmod 755 *.sh
./submit-many_slurm_BEAST_A7_bk98_01_13_beast_no_beagle.sh

cd ..
cd BEAST_A7_bk98_01_13E/
chmod 755 *.sh
./submit-many_slurm_BEAST_A7_bk98_01_13E_beast_no_beagle.sh
