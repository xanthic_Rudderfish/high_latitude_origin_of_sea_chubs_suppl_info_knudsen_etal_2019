#!/bin/bash
#SBATCH -J BEAST_A7_bk98_01_13E
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/BEAST_A7_bk98_01_13/BEAST_A7_bk98_01_13E
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_lc_BEAST_A7_bk98_01_13E.txt
#SBATCH -e stderr_lc_BEAST_A7_bk98_01_13E.txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -trees *.species.trees stb_kyp_094_06_comb3.trees
srun logcombiner -trees -burnin 40000000 *species.trees BEAST_A7_bk98_01_13E_comb3.trees
