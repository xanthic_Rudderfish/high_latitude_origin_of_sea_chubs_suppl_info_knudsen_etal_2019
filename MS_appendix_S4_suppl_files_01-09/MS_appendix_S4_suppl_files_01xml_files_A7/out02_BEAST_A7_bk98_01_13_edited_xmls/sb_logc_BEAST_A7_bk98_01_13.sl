#!/bin/bash
#SBATCH -J BEAST_A7_bk98_01_13
#SBATCH -A uoa00029         # Project Account
#SBATCH -D /nesi/project/uoa00029/runs/BEAST_A7_bk98_01_13/BEAST_A7_bk98_01_13
#SBATCH --time=2:00:00     # Walltime
#SBATCH --mem-per-cpu=4096  # memory/cpu (in MB)
#SBATCH --tasks-per-node=1
#SBATCH --mail-type=ALL
# #SBATCH --mail-user=sknu003@aucklanduni.ac.nz
#SBATCH -o stdout_logc_BEAST_A7_bk98_01_13.txt
#SBATCH -e stderr_logc_BEAST_A7_bk98_01_13.txt

#module load BEAST/1.8.0
module load BEAST/1.8.4-gimkl-2017a-no-beagle
## module load logcombiner
## srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner *.log stb_kyp_094_06E_comb.log
#srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 20000000 *.log stb_kyp_094_06E_comb.log
#srun /share/apps/BEAST/noarch/1.8.0/bin/logcombiner -burnin 6000000 *.log btobis_15_08_86_GTR_comb.log
srun logcombiner -burnin 40000000 *.log BEAST_A7_bk98_01_13_comb.log
