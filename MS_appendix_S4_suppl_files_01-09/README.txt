READ-ME FILE 

for the supplementary input-files provided for :

Title:
The herbivorous fish family Kyphosidae (Teleostei: Perciformes) represents a recent radiation from higher latitudes

by the Authors:
Steen Wilhelm Knudsen, J. Howard Choat and Kendall D. Clements

To be published in Journal of Biogeography

CITATION:
If you use any of the code provided in this bitbucket repository - then ...

Please cite:
Knudsen, S.W., Choat, J.H., Clements, K.D. (submitted). The herbivorous fish family Kyphosidae (Teleostei: Perciformes) represents a recent radiation from higher latitudes. Journal of Biogeography vol. xx, pp. xx-xx.


This data repository holds the directory:
MS_appendix_S4_suppl_files_01-09

Which holds the subdirectories:
MS_appendix_S4_suppl_files_01xml_files_A7
MS_appendix_S4_suppl_files_02xml_files_starBEAST
MS_appendix_S4_suppl_files_03xml_files_BCD
MS_appendix_S4_suppl_files_04_mrbayes_files_for_figS3.5_S.3.12
MS_appendix_S4_suppl_files_05_bash_code_assemble_dataset_BDC
MS_appendix_S4_suppl_files_06_bash_code_assemble_dataset_for_figS3.5_S3.12
MS_appendix_S4_suppl_files_07_R_code_BioGeoBEARS
MS_appendix_S4_suppl_files_08_R_code_phyloch


The code and input files are set up to run in a unix terminal with bash:
4.3.30(1)-release
You will also need Perl and R installed for some pieces of the code.
The R-code have been tested in R v3.2.1 and R v3.5.1. The 'treeio' package (Yu, 2019) requires at least R v3.5.1, but the 'phyloch' package (Heibl, 2008) could not work in R v3.5.1 and was instead used in R v3.2.1. The 'phyloch' package also required a lot of zip-files available locally, please refer to the guide on 'phyloch' provided by  Heibl (2008). All of the biogeographical analysis using the 'BioGeoBEARS' package (Matzke, 2013) requires R v3.5.1.

If you want to rerun the analysis in MrBayes and BEAST, you will also need access to a remote server, as the analysis are too demanding for a local desktop computer.

The subdirectories listed above holds input files for MrBayes v3.2.6 x64 (Ronquist & Huelsenbeck, 2003) and BEAST v.1.8.4. (Drummond et al., 2012) Preparing the input files also makes use of PartitionFinder (Lanfear et al., 2012) for finding the most optimal nucleotide substitution models among 56 models available for MrBayes v.3.2.6.
Preparing input nexus-files for MrBayes and for BEAST also makes use of FASconCAT-G (Kück, 2009). FASconCAT-G is a nice perl code that enables RY (purine/pyrimidine) -recoding of third codon positions, and enables you to assemble different partitions in to one large matrix to be used in a nexus file.

The R-code makes use of various packages. These are listed in the supplementary material associated with the main publication: 
Knudsen, S.W., Choat, J.H., Clements, K.D. (submitted). The herbivorous fish family Kyphosidae (Teleostei: Perciformes) represents a recent radiation from higher latitudes. Journal of Biogeography vol. xx, pp. xx-xx.

The idea behind this data and code repository is that it may allow readers to rerun the mrbayes analysis on input nexus-files to prepare the same phylogenetic trees as presented in our paper, and rerun the same xml-files.

The additional code for bash and for R should also allow the reader to go from raw alignment-FASTA-files and rename the sequences, and evaluate nucleotide saturation on third codon positions, and re-assemble partitions with re-coded and non-recoded partitions in a nexus-file. The nexus- and phylip files can also be tested in PartitionFinder in a greedy search for the most optimal partitioning scheme and most optimal substitution models among the 56 models available for MrBayes and BEAST.

The R-code for BioGeoeBEARS and for phyloch will allow the reader to experiment with inferring  ancestral oceanic distribution on topologies (with BioGeoBEARS), and to rescale chronotrees with geological epochs (using Phyloch).

##########################################################################################
IMPORTANT:
Much of this code was set up to work locally, and to be submitted to remote servers for the more demanding computational tasks (i.e. running analysis in PartitionFinder , MrBayes and BEAST). This means that a lot of the submission files the code will generate will only work on my local remote node made available to me by the Auckland University Computer Science department. To rerun these parts that are to be analysed remotely, you will need a similar remote node you can logon to using a terminal, and a filetransfer client (e.g. Filezilla). 
A lot of the working directories in the individual bash and R codes are also specific to my own computer, and you will need to edit these. But for the major parts of codes I have tried to setup the code so it will perform equally well in any working directory.
Please examine each piece of code carefully in a text-editor and modify it to match your own computer, and your own remote node. 
##########################################################################################



The first three subdirectories:
MS_appendix_S4_suppl_files_01xml_files_A7
MS_appendix_S4_suppl_files_02xml_files_starBEAST
MS_appendix_S4_suppl_files_03xml_files_BCD

Holds the input xml-files used for the BEAST analysis in BEAST v.1.8.4.
These can be used directly in BEAST v.1.8.4 to generate the same topologies as presented in our paper. These xml-files should be able to run directly, and do not need any modifications and renaming of working directories.

The xml-file in the first folder (MS_appendix_S4_suppl_files_01xml_files_A7) is identical to the xml-file associted with our previous study on Kyphosidae (Knudsen and Clements 2016a) and the supplementary files linked to our data-publication (Knudsen and Clements 2016b). 

The xml-file provided in the second folder (MS_appendix_S4_suppl_files_02xml_files_starBEAST) can be run directly in BEAST and will recreate the starBEAST topology presented in our paper.

The third folder (MS_appendix_S4_suppl_files_03xml_files_BCD) holds the input files for the supplementary chronotrees (Figure S3.1-S3.4) with reduced datasets - i.e. referred to as dataset B, C and D in the supplementary material. Running these and combining all tree-files and getting consensus trees will recreate the same topologies as presented in the supplementary material (Figure S3.1-S3.4).

The fourth folder (MS_appendix_S4_suppl_files_04_mrbayes_files_for_figS3.4_S.3.11) holds two tar.gz files. One has the input-files for checking the most optimal partitioning scheme and substitution models in PartitionFinder. The second tar.gz files holds all the nexus-files needed for running the MrBayes analysis presented in the supplementary material as Figure S.3.5-S3.11. The submission scripts are specific to my remote node at the Auckland University, and you will need to change these parts to specify your own working directories where your runs can be performed remotely.
But the nexus-files can be run directly in MrBayes.
The input-files for PartititionFinder can also be run directly in PartititionFinder.

The fifth folder (MS_appendix_S4_suppl_files_05_bash_code_assemble_dataset_BDC) holds 9 bash codes I have prepared.
The folder also contains the alignments I used to have in an excel spreadsheet. Each of the 9 bash codes can be run separately to transform alignment files, in to RY-recoded fasta-files, and to check the level of nucleotide saturation with an R-code. The RY-recoding and the assembly of a larger matrix is performed with FASconCAT-G (Kück, 2009).
FASconCAT-G also generates a phylip-file that can used as an input-file for PartitionFinder (Lanfear et al., 2012).
Running these bashcodes in order will prepare submission files for first PartitionFinder, which then can be run remotely.
Each bash code produces output-folders. You will need to delete these outputfolders if you want to re-run any of the bashcodes.
I forgot to include a line in these pieces of bashcode that overwrites the previous versions of the output folders.
I included the resulting output-folders to enable the reader to inspect what the resulting output should look like if the bashcodes are working as intended.
The results from PartitionFinder can be dowloaded from the remote server, and used as input in the last bashcode: "bashpart08_mrbayres_nexus_w_partition_finder_division01.sh".
This last code will make use of the nexus-files generated from FASconCAT-G and use the optimal partitioning shceme and substitutition models inferred with PartitionFinder, and use this to create nexus files that can be used as input for direct analysis in MrBayes, or be imported in to BEAuti v.1.8.4, to set up an xml-file  to be analysed in BEAST v1.8.4.
If you inspect the resulting nexus-files generated from this last bashcode you should be able to identify the different blocks and settings that are required for a MrBayes analysis. You need a lot of familiarity with the mrbayes-block required in a nexus-file for an analysis in MrBayes. Consult the examples provided with MrBayes to see what how a mrbayes-block can be prepared, and compare this with the mulitple blocks in these output-files generated by this last bashcode.
The bashcodes can be altered to fit your own data. But this will require a bit of tinkering and experience with bash.
There are a lot of repetitive elements in these bash codes, as I am still trying to get familiar with bash and coding loops and functions. In other words, the code provided here can certainly be improved and optimized. But I do not have the time for such tinkering with these pieces of code at the time. I will leave it up to anyone else who wants to give this a try.
If I do manage to make better versions of the bash codes, I will try and make these available too on this bitbucket account. Or at least provide directions on how another version of the code can be obtained. But as for now, the code is provided as it is.

The sixth folder (MS_appendix_S4_suppl_files_06_bash_code_assemble_dataset_for_figS3.5_S3.12) holds the input alignment files used for generating the supplementary topologies presented in the supplementary material (i.e. Figure S3.5 to S3.12). It also includes the bash codes for transforming these alignments into nexus files, and checking for third codon position saturation, and performing a test of the most optimal nucleotide substitution model (among the 56 models available for MrBayes) and including the best partitioning shceme and the best nucleotide substitution models in a nexus input-file for Mrabyes.
Each bash code should be run individually, and the output from each bash code must be inspected, before continuing with the next bashcode.
These pieces of bash code also rely on access to a remote server, where the analysis in PartitionFInder and MrBayes can be performed.

The seventh folder (MS_appendix_S4_suppl_files_07_R_code_BioGeoBEARS) holds the input files used to prepare the phylogeographic analysis and reconstruction of ancestral distributions as presented in the main paper (Fig. 2). This code is directly adopted from the instructions provided with the BioGeoBEARS R-package.
The BioGeoBEARS package depends on a local directory where all the different packages for BioGeoBEARS and the input-topology and the character-matrix has to be provided. You should examine the examples and detailed tutorials provided with BioGeoBEARS, before trying to re-run this R-code. The R-code can be run locally in R-studio.

The eight folder (MS_appendix_S4_suppl_files_08_R_code_phyloch) contains an R-code that can be used on the chronotrees obtained with BEAST v.1.8.4. The phyloch packages makes it possible to re-scale the topology to geological epochs. This re-scaled topology can then be exported as a pdf-file or as an .svg-file. I have not worked out how to inlcude the 95% HPD-intervals. The solution I ended up with was to export the topology with 95% HPD intervals from FigTree v.1.4 (Rambaut, 2009) an opening this file in Adobe Illustrator.
The FigTree file could then be overlaid on the phyloch-modified topology, to produce the topologies presented ad Fig. 2-5 and Fig. S3.1-S3.4.




References:
Drummond, A. J., Ho, S. Y. W., Phillips, M. J., & Rambaut, A. (2006). Relaxed phylogenetics and dating with confidence. PLoS Biology, 4, e88.

Heibl, C. (2008). PHYLOCH: R language tree plotting tools and interfaces to diverse phylogenetic software packages. http://www.christophheibl.de/Rpackages.html.

Knudsen, S. W. & Clements, K. D. (2016a). World-wide species distributions in the family Kyphosidae (Teleostei: Perciformes). Molecular Phylogenetics and Evolution, 101, 252–266.

Knudsen, S. W. & Clements, K. D. (2016b). Input data for inferring species distributions in Kyphosidae world-wide. Data in Brief, 8, 1013–1017.

Kück, P. (2009). FASconCAT, version 1.0, Zool. Forshcungsmuseum A. Koenig, Germany.

Lanfear, R., Calcott, B., Ho, S. Y. W. & Guindon, S. (2012). PartitionFinder: combined selection of partitioning schemes and substitution models for phylogenetic analyses. Molecular Biology and Evolution, 29, 1695–1701. http://dx.doi.org/10.1093/molbev/mss020

Matzke, N. J. (2013). BioGeoBEARS: BioGeography with Bayesian (and Likelihood) Evolutionary Analysis in R Scripts. R package, version 0.2.1, published July 27, 2013 at: http://CRAN.R-project.org/package=BioGeoBEARS

Rambaut A. (2009). FigTree 1.4.1, a graphical viewer of phylogenetic trees. Available from http://tree.bio.ed.ac.uk/software/figtree/

R Core Team, (2018). R: A Language and Environment for Statistical Computing. R Foundation for Statistical Computing. https://www.R-project.org

Ronquist, F., & Huelsenbeck, J. P. (2003). MRBAYES 3: Bayesian phylogenetic inference under mixed models. Bioinformatics, 19, 1572-1574.

Yu, G. (2019). treeio: Base Classes and Functions for Phylogenetic Tree Input and Output. R package version 1.6.2. https://guangchuangyu.github.io/software/treeio
