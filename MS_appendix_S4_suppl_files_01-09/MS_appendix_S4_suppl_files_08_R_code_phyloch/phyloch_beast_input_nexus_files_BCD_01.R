
#____________________________________________________________________________#
# R-code provided for the research article:
#
# Title: 
# The herbivorous fish family Kyphosidae (Teleostei: Perciformes) represents a rapid and recent radiation from higher latitudes
#
# Authors:
# Steen W. Knudsen, Howard J. Choat and Kendall D. Clements
# 
####################################################################################
# Listed as Appendix S8 in the main text
####################################################################################
#
#
#READ THE SECTION BELOW FIRST, BEFORE EXECUTING THE R-CODE
#
#
####################################################################################
# Change the working directory to a path on your own computer , and run
# the individual parts below to reproduce the diagrams presented in the paper
#
# IMPORTANT !!!
#
#
#
#This R-code can be executed in R-studio : Version 0.98.994 – © 2009-2013 RStudio, Inc.
#
#____________________________________________________________________________#

#Also see this webpage:
#http://www.kmeverson.org/blog/plot-a-beautiful-tree-with-geological-timescale-in-r


#You'll need two packages: phyloch (if you're reading in a BEAST tree) and strap. Note that phyloch is not available in CRAN and must be downloaded from the author's website. Here's the R code for making this pretty tree:
#website for phyloch: http://www.christophheibl.de/Rpackages.html

install.packages("phyloch")
install.packages("/Users/steenknudsen/r_test_eDNA/phyloch_1.5-3.tgz", repos = NULL)
install.packages("strap")
install.packages("/Users/steenknudsen/r_test_eDNA/strap_1.4.tar.gz", repos = NULL)
install.packages("geoscale")
install.packages("/Users/steenknudsen/r_test_eDNA/geoscale_2.0.tar.gz", repos = NULL)
install.packages("devtools")
devtools::install_github("beast-dev/RBeast")
library(devtools)
install_github("olli0601/rBEAST")
install.packages("ape")
install.packages("colorspace")
install.packages("XML")
install.packages("phytools")
install.packages("OutbreakTools")
install.packages("paleotree")

library(phyloch)
library(strap)
library(geoscale)
library(OutbreakTools)
library(ape)
library(colorspace)
library(XML)
library(phytools)
library(paleotree)

# set working directory
setwd ("/Users/steenknudsen/Documents/Documents/Kyphosidae/MS_9_Phylogeny_paper2/beast_runs_suppl_matr_dataset_BCD/beast_input_nexus_files_datasetB_3")
getwd()

#t <- read.annotated.nexus("beast_ophidiiform_v04_02_phyloch_test.tre") 
t <- read.annotated.nexus("beast_input08_08_B17_RC_GTR_mean_tree.tre") 
class(t)
#https://cran.r-project.org/web/packages/ape/ape.pdf
#order the nodes in decreasing order - i.e. ladderize right
t <- ladderize(t, right = TRUE)

t$root.time <- t$root.annotation$height
###This command is really flexible. "units" allows you to select whether or not to show Periods, Epochs, Ages, Eons, or a user-defined timescale. "boxes" allows you to pick which unit to plot gray boxes on. "width" controls the thickness of the branches. "x.lim" controls the year span. Yes, I know that the labels are squished on the time axis, but you can adjust those.
geoscalePhylo(tree=t, units=c("Period", "Epoch", "Age"), direction="rightwards", boxes="Epoch", cex.tip=0.5, cex.age=0.7, cex.ts=0.7, label.offset=0, x.lim=c(-0,150), lwd=3, width=1)

# plot the tree 
plot (t)

# set working directory
setwd ("/Users/steenknudsen/Documents/Documents/Kyphosidae/MS_9_Phylogeny_paper2/beast_runs_suppl_matr_dataset_BCD/beast_input_nexus_files_datasetC_3")
getwd()

#t <- read.annotated.nexus("beast_ophidiiform_v04_02_phyloch_test.tre") 
t <- read.annotated.nexus("beast_input08_08_C22_RC_GTR_mean_tree.tre") 
class(t)
#https://cran.r-project.org/web/packages/ape/ape.pdf
#order the nodes in decreasing order - i.e. ladderize right
t <- ladderize(t, right = TRUE)

t$root.time <- t$root.annotation$height
###This command is really flexible. "units" allows you to select whether or not to show Periods, Epochs, Ages, Eons, or a user-defined timescale. "boxes" allows you to pick which unit to plot gray boxes on. "width" controls the thickness of the branches. "x.lim" controls the year span. Yes, I know that the labels are squished on the time axis, but you can adjust those.
geoscalePhylo(tree=t, units=c("Period", "Epoch", "Age"), direction="rightwards", boxes="Epoch", cex.tip=0.5, cex.age=0.7, cex.ts=0.7, label.offset=0, x.lim=c(-0,110), lwd=3, width=1)




# set working directory
setwd ("/Users/steenknudsen/Documents/Documents/Kyphosidae/MS_9_Phylogeny_paper2/beast_runs_suppl_matr_dataset_BCD/beast_input_nexus_files_datasetD_3")
getwd()

#t <- read.annotated.nexus("beast_ophidiiform_v04_02_phyloch_test.tre") 
t <- read.annotated.nexus("beast_input08_08_D41_RC_GTR_mean_tree.tre") 
class(t)
#https://cran.r-project.org/web/packages/ape/ape.pdf
#order the nodes in decreasing order - i.e. ladderize right
t <- ladderize(t, right = TRUE)

t$root.time <- t$root.annotation$height
###This command is really flexible. "units" allows you to select whether or not to show Periods, Epochs, Ages, Eons, or a user-defined timescale. "boxes" allows you to pick which unit to plot gray boxes on. "width" controls the thickness of the branches. "x.lim" controls the year span. Yes, I know that the labels are squished on the time axis, but you can adjust those.
geoscalePhylo(tree=t, units=c("Period", "Epoch", "Age"), direction="rightwards", boxes="Epoch", cex.tip=0.5, cex.age=0.7, cex.ts=0.7, label.offset=0, x.lim=c(-0,110), lwd=3, width=1)

# add node labels
nodelabels()

#findMRCA(tree, tips=c("sp1", "sp2", "sp3"))

## from this website:
## http://nickcrouch.github.io/r_pages/make.node.age.df/make.node.age.df.html
## ____________use this function____________function start________________________________

##	make.node.age.df <- function(phy){
##	
##	annotations <- phy$annotations
##	
##	raw.heights <- lapply(annotations, get_height)
##	
##	heights <- do.call(rbind, raw.heights)
##	
##	heights$node <- phy$edge[,2]
##	
##	is.tip <- phy$edge[,2] <= Ntip(phy)
##	
##	node.heights <- heights[is.tip==FALSE,]
##	
##	# get the root
##	root <- as.data.frame(rbind(unlist(phy$root.annotation$`height_95%_HPD`)))
##	root$node <- Ntip(phy) + 1
##	colnames(root)[1:2] <- c("Lower95%", "Upper95%")
##	
##	# all results
##	export <- rbind(root, node.heights)
##	
##	return(export)
##	
##	}
##	
##	
##	###
##	
##	
##	get_height <- function(x){
##	h <- x$`height_95%_HPD`
##	df <- as.data.frame(rbind(unlist(h)))
##	colnames(df) <- c("Lower95%", "Upper95%")
##	return(df)
##	}
## ____________use this function____________function_end_________________________________


## library(OutbreakTools)

## Loading required package: ggplot2

## Loading required package: network

## network: Classes for Relational Data
## Version 1.13.0 created on 2015-08-31.
## copyright (c) 2005, Carter T. Butts, University of California-Irvine
##                     Mark S. Handcock, University of California -- Los Angeles
##                     David R. Hunter, Penn State University
##                     Martina Morris, University of Washington
##                     Skye Bender-deMoll, University of Washington
##  For citation information, type citation("network").
##  Type help("network-package") to get started.

## Loading required package: knitr

##  OutbreakTools 0.1-14 has been loaded

library(ape)

source("make.node.age.df.R")

phy <- read.annotated.nexus("beast_ophidiiform_v04_02_phyloch_test.tre")

node.ages <- make.node.age.df(phy)

node.ages


#Now add the HPD intervals
#http://nickcrouch.github.io/r_pages/add_HPD_info/add.HPD.info.html

library(phyloch)
## Loading required package: ape
## Loading required package: colorspace
## Loading required package: XML
library(ape)
library(phytools)

## Loading required package: maps
## 
##  # ATTENTION: maps v3.0 has an updated 'world' map.        #
##  # Many country borders and names have changed since 1990. #
##  # Type '?world' or 'news(package="maps")'. See README_v3. #

library(strap)

## Loading required package: geoscale

source("make_HPD_node_plot.R")
#phy <- read.nexus("example.phylo.nex")
#load("species_group_info.RData")

#2) Find the node(s) using findMRCA (from the package phytools)
#findMRCA(tree, tips=c("sp1", "sp2", "sp3"))

# This function requires that "node", "min" and "max" appear in the column names in some form
#age_data <- as.data.frame(matrix(NA, ncol=3, nrow=3))
#colnames(age_data) <- c("Genus", "Minimum", "Maximum")
#age_data[1,] <- c("Genus_A", 20 ,35 )
#age_data[2,] <- c("Genus_B", 17, 28)
#age_data[3,] <- c("Genus_C", 22, 35)

#age_data[,2] <- as.numeric(age_data[,2])
#age_data[,3] <- as.numeric(age_data[,3])

#age_data

##     Genus Minimum Maximum
## 1 Genus_A      20      35
## 2 Genus_B      17      28
## 3 Genus_C      22      35

#The first function provided (find.nodes) will identify the most recent common ancestor for each specified group. The nodes identified are where the age data will subsequently be plotted.
#find.nodes requires:
  
#  * data: A `data.frame` containing the minimum, and and maximum ages, as well as the corresponding species group names
#* phy: An object of class `phylo`
#* spp_group_list: An object of class `list` where the names match the group names in `data`, and each element contains species found in `phy$tip.label`
#* colname: A vector of class `character` and length 1. Specify the column name in `data` that correponds to the species group data

#Using the previously defined age data, and loaded species group data:
age.data.inc.nodes <- find.nodes(age_data, phy, species_group_info, colname = "Genus")

#Now we have a data.frame with our age data, and the corresponding node in the phylogeny
age.data.inc.nodes

##     Genus Minimum Maximum Node
## 1 Genus_A      20      35   23
## 2 Genus_B      17      28   31
## 3 Genus_C      22      35   29

#The previously generated data can now be used to generate our plot:
  make_HPD_node_plot(phy=phy, data=age.data.inc.nodes, 
                   line.width=5, node.col = "lightblue",line.col = "red",node.pch=21, node.cex=2)






HPDbars(phy, col="red")

class(t)
str(t)

t[6]
#Make HPD node plot- Add probability density information into phylogenies
#http://nickcrouch.github.io/add_HPD_info/add.HPD.info.html
#http://nickcrouch.github.io/r_pages/add_HPD_info/add.HPD.info.html
source("make_HPD_node_plot.R")
phy <- read.annotated.nexus("beast_ophidiiform_v04_02_phyloch_test.tre") 
phy$root.time <- phy$root.annotation$height
phy$root.time.height_95percent <- phy$root.annotation$`height_95%_HPD`

phy$annotations.height_95percent <- phy$annotation$`height_95%_HPD`
load("species_group_info.RData")

phy_annotations <- phy$annotations
phy_annotation_names <- names(phy$annotations[[1]])

phy$tip.label
phy$Nnode
phy$edge.length

tip_list <- as.list(as.vector(phy$tip.label))
tip_vector <- as.vector(phy$tip.label)

for (i in phy$annotations[[i]]$`height_95%_HPD`) {
  if (!i %% 2){
    next
  }
  print(i)
}


phy_len <- length(phy$annotations)

phy_phylo_labnames <- names(phy)

max.tree.height <- max(nodeHeights(phy))

phy_annotations_df <- as.data.frame(phy$annotations)

phy_nodeheights<- nodeHeights(phy)
age_data <- as.data.frame(phy_nodeheights)
colnames(age_data) <- c("minimum", "maximum")

phy$'height_95%_HPD' <- phy$annotations

#phy_height_95_HPD <- phy$annotations$[["`height_95%_HPD`"]]
View(phy)
# This function requires that "node", "min" and "max" appear in the column names in some form
spp_group_list <- phy$tip.label
class(phy)


phy[7]
head(phy[6])
phy[4]
phy[1]
phy$edge
phy[2]
phy[3]
phy[5]
phy['height_95%_HPD']


age_data <- as.data.frame(matrix(NA, ncol=3, nrow=3))
colnames(age_data) <- c("Genus", "Minimum", "Maximum")

plot.phylo(t2) 



#Compare this to a tree made with the simple plot command:

plot(t2, show.tip.label=F)
axisPhylo()
#_____________
# check out if this webpage is any help:
#https://pseudoplocephalus.com/2017/02/15/so-you-want-to-make-a-time-calibrated-phylogenetic-tree/
  
tree_ophidiform<-read.tree("beast_ophidiiform_v04_02_phyloch_test.tre")
plot(tree_ophidiform)


#_____________
# check out if this webpage is any help:
#http://nemagraptus.blogspot.dk/2014/07/creating-pretty-plots-of-trees.html

# from http://www.rdocumentation.org/packages/strap/versions/1.4/topics/geoscalePhylo

### Example lungfish data
data(Dipnoi)

tree_l <- DatePhylo(Dipnoi$tree, Dipnoi$ages, method="equal", rlen=1)


geoscalePhylo(tree=tree_l, boxes="Age", cex.tip=0.4)

# Plotting the tree with the stratigraphical ranges included

geoscalePhylo(tree=tree_l, ages=Dipnoi$ages, boxes="Age", cex.tip=0.4)

# Including all temporal units into the stratigraphic column

geoscalePhylo(tree_l, Dipnoi$ages, units = c("Eon","Era","Period","Epoch","Age"),
              boxes="Age", cex.tip=0.4)

# Plotting the numerical values on the time scale at Age resolution

geoscalePhylo(tree_l, Dipnoi$ages, units = c("Eon","Era","Period","Epoch","Age"),
              boxes="Age", cex.tip=0.4,tick.scale="Age")

### Example trilobite data
data(Asaphidae)

tree_a <- DatePhylo(Asaphidae$trees[[1]], Asaphidae$ages, method="equal", rlen=1)

geoscalePhylo(ladderize(tree_a), Asaphidae$ages, boxes="Age", x.lim=c(504,435), 
              cex.tip=0.5, cex.ts=0.5,vers="ICS2009") 

# Plotting the tree vertically

geoscalePhylo(ladderize(tree_a), Asaphidae$ages, boxes="Age", x.lim=c(504,435), 
              cex.tip=0.5, cex.ts=0.5,direction="upwards",vers="ICS2009") 

# Including a user-defined time scale

data(UKzones)
data(Asaphidae)

tree_a <- DatePhylo(Asaphidae$trees[[1]], Asaphidae$ages, method="equal", rlen=1)

geoscalePhylo(ladderize(tree_a), Asaphidae$ages, units = c("Eon","Era","Period",
                                                           "Epoch","User"), boxes="Age", cex.tip=0.4,user.scale=UKzones,
              vers="ICS2009",cex.ts=0.5,x.lim=c(440),direction="upwards")

# Rotating the text on the time scale

tree_a <- DatePhylo(Asaphidae$trees[[1]], Asaphidae$ages, method="equal", rlen=1)

geoscalePhylo(ladderize(tree_a), Asaphidae$ages, units = c("Period",
                                                           "Epoch","Age","User"), boxes="Age", cex.tip=0.4,user.scale=UKzones,
              vers="ICS2009",cex.ts=0.5,x.lim=c(440),arotate=0,erotate=0,urotate=0)